@extends('layouts.main_app')
@include('navbar.navbar')
@if (session('status'))
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <center><strong>{{ session('status') }}</strong></center>
                </div>
            </div>
        </div>
    </div>
@endif
<div style="background: url(../images/fon_2.jpg)">
    <div class="row mt-5">
        <div class="col-md-12">
            <center><h1>Svit Parkety</h1></center>
            <center>
                <div class="mt-5"><h3>Змінити позицію</h3></div>
            </center>
        </div>
    </div>

    <div class="container">
        {{Form::open(['route' => 'edit', 'files' => true])}}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Назва</label> <input type="text" class="form-control" rows="1" name="name"
                                                id="name"
                                                value="{{$item->name}}"
                                                placeholder="Name of Item"> </input>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Колеція</label> <input type="text" class="form-control" rows="1" name="collection"
                                                id="collection"
                                                value="{{$item->collection}}"
                                                placeholder="Name of Item"> </input>
                </div>
            </div>
        </div>

        <input type="hidden" name="item_id" value="{{$item->id}}">
        <input type="hidden" name="old_url" value="{{$old_url}}">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <p><textarea rows="6" cols="74" name="text">{{$item->description}}</textarea></p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Quizzes">Для відображення потрібної валюти значення має бути більше О, інші рівні О</label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Quizzes">Ціна EUR</label> <input type="text" class="form-control" rows="1" name="EUR"
                                                            id="EUR"
                                                            value="{{$item->EUR}}"
                                                            placeholder="Price of Item"> </input>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Quizzes">Ціна UAN</label> <input type="text" class="form-control" rows="1" name="UAN"
                                                            id="UAN"
                                                            value="{{$item->UAN}}"
                                                            placeholder="Price of Item"> </input>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Quizzes">* Усі поля обовязкові до заповнення</label>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Знаходиться зараз в категорії: <b>{{$item_category}}</b></label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="end_date" class="col-sm-3 col-form-label">Відноситься до категорії</label>
            <div class="col-sm-8">
                <table>
                    <tr>
                    @foreach ($category as $cat)
                        <tr>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label" for="{{ $cat->id }}"> <input
                                            class="form-check-input" type="checkbox"
                                            id="{{ $cat->id }}"
                                            value="{{ $cat->id }}"
                                            name="category">
                                    {{ $cat->name }}</label>
                            </div>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>

        {{Form::label('image', 'Зображення',['class' => 'control-label'])}}
        {{Form::file('image')}}

        {{Form::submit('Змінити', ['class' => 'buttons'])}}

        {{Form::close()}}
    </div>

</div>


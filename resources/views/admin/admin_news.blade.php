@extends('layouts.main_app')
@include('navbar.navbar')
@if (session('status'))
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <center><strong>{{ session('status') }}</strong></center>
                </div>
            </div>
        </div>
    </div>
@endif
<div style="background: url(../images/fon_2.jpg)">
    <div class="row mt-5">
        <div class="col-md-12">
            <center><h1>Svit Parkety</h1></center>
            <center>
                <div class="mt-5"><h3>Створити новину</h3></div>
            </center>
        </div>
    </div>

    <div class="container">
        {{Form::open(['route' => 'news.create'])}}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Назва</label> <input type="text" class="form-control" rows="1" name="tittle"
                                                              id="name"
                                                              value=""
                                                              placeholder="Tittle of News"> </input>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Quizzes">Опис</label>
                    <textarea name="description" cols="150" rows="3"></textarea>
                </div>
            </div>
        </div>

        {{Form::submit('Створити', ['class' => 'buttons'])}}

        {{Form::close()}}

        <div class="row mt-5">
            <div class="col-md-12">
                <center>
                    <div class="mt-5"><h3>Новини на сайті</h3></div>
                </center>
            </div>
        </div>
            <div class="container text-center">
                <div class="col-md-12">
                    @foreach($news as $new)
                        <div class="row">
                    <div class="col-md-6 mt-3">
                        <h2>{!! $new->tittle !!}</h2>
                    </div>
                    <div class="col-md-6 mt-3 ">
                        <form method="POST" action="{{ action('AdminController@delete_news') }}">
                            <input type="hidden" name="news_id" value="{{$new->id}}">
                            <button type="sumbit" class="btn btn-outline-danger btn-sm">Delete</button>
                            {{ csrf_field() }}
                        </form>
                    </div>
                        </div>
                    @endforeach
                </div>
            </div>
    </div>

</div>


@extends('layouts.main_app')
@include('navbar.navbar')
@if (session('status'))
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <center><strong>{{ session('status') }}</strong></center>
                </div>
            </div>
        </div>
    </div>
@endif
<div style="background: url(../images/fon_2.jpg)">
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <center><h1>Svit Parkety</h1></center>
                <center>
                    <div class="mt-5"><h3>Курс на сайті</h3></div>
                </center>
            </div>
        </div>

        <div class="col-md-12 mt-5">
            <div class="row">
                <div class="col-md-6 text-center">
                    <h2>Курс EUR</h2>
                </div>
                <div class="col-md-6 text-center">
                    <h2>{{$kurs->EUR}}</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 text-center">
                    <h2>Курс USD</h2>
                </div>
                <div class="col-md-6 text-center">
                    <h2>{{$kurs->USD}}</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 text-center">
                    <h2>Курс UAN</h2>
                </div>
                <div class="col-md-6 text-center">
                    <h2>{{$kurs->UAN}}</h2>
                </div>
            </div>

        </div>
    </div>

    <div class="container">
        <div class="col-md-12">
            <center>
                <div class="mt-5"><h3>Задати нове значення курсу</h3></div>
            </center>
        </div>
        {{Form::open(['route' => 'change_rate'])}}
        <div class="col-md-12 mt-5">
            <div class="row">
                <div class="col-md-6 text-center">
                    <h2>EUR</h2>
                </div>
                <div class="col-md-6 text-center">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control" rows="1" name="EUR"
                                   id="EUR"
                                   value=""
                                   placeholder="new rate"> </input>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 text-center">
                    <h2>USD</h2>
                </div>
                <div class="col-md-6 justify-content-center">
                    <div class="col-md-12">

                        <div class="form-group">
                            <input type="text" class="form-control" rows="1" name="USD"
                                   id="USD"
                                   value=""
                                   placeholder="new rate"> </input>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 text-center">
                    <h2>UAN</h2>
                </div>
                <div class="col-md-6 text-center">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control" rows="1" name="UAN"
                                   id="UAN"
                                   value=""
                                   placeholder="new rate"> </input>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="form-group">
                        <label>* Усі поля обовязкові до заповнення</label>
                    </div>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-md-12 text-center">
                    {{Form::submit('Змінити', ['class' => 'button'])}}
                </div>
            </div>
        </div>
    </div>
</div>


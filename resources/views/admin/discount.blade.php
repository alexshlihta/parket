@extends('layouts.main_app')
@include('navbar.navbar')
@if (session('status'))
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <center><strong>{{ session('status') }}</strong></center>
                </div>
            </div>
        </div>
    </div>
@endif
<div style="background: url(../images/fon_2.jpg)">
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <center><h1>Svit Parkety</h1></center>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="col-md-12">
            <center>
                <div class="mt-5"><h3>Задати значення знижки</h3></div>
            </center>
        </div>
        {{Form::open(['route' => 'discount_create'])}}
        <div class="col-md-12 mt-5">
            <div class="row">
                <div class="col-md-6 text-center">
                    <h2>Ціна без знижки, EUR</h2>
                </div>
                <div class="col-md-6 text-center">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h2>{{$item->EUR}} eur</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 text-center">
                    <h2>Ціна без знижки, UAN</h2>
                </div>
                <div class="col-md-6 text-center">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h2>{{$item->UAN}} uan</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 text-center">
                    <h2>Знижка на данний момент</h2>
                </div>
                <div class="col-md-6 text-center">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h2>{{$item->discount}} %</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 text-center">
                    <h2>Встановити знижку, %</h2>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12">

                        <div class="form-group ">
                            <input type="hidden" name="item_id" value="{{$item->id}}">
                            <input type="text" class="form-control" rows="1" name="discount"
                                   id="discount"
                                   value=""
                                   placeholder="new rate"> </input>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="form-group">
                        <label>* Усі поля обовязкові до заповнення</label>
                    </div>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-md-12 text-center">
                    {{Form::submit('Встановити', ['class' => 'button'])}}
                </div>
            </div>
        </div>
    </div>
</div>


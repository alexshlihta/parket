@section('title', 'Паркетна дошка в наявності у Вінниці – вигідна пропозиція. Вибирайте паркетну дошку в каталозі під Ваш
    смак')
@include('header.header')
@include('main_content.menu')
<h2 class="text-center mt-5 mb-5">ФОТОГАЛЕРЕЯ</h2>

<div class="container">
	<div class="col-md-12">
		<div class="row mb-5">
			<div class="col-md-6">
				<a data-fancybox="gallery"
				   href="images/works/DSCF0156.jpg">
					<img class="gallery_img" src="images/works/DSCF0156.jpg"></a>
			</div>
			<div class="col-md-6">
				<a data-fancybox="gallery"
				   href="images/works/DSCF0198.jpg">
					<img class="gallery_img" src="images/works/DSCF0198.jpg"></a>
			</div>
		</div>

		<div class="row mb-5">
			<div class="col-md-6">
				<a data-fancybox="gallery"
				   href="images/works/DSCF0210.jpg">
					<img class="gallery_img" src="images/works/DSCF0210.jpg"></a>
			</div>
			<div class="col-md-6">
				<a data-fancybox="gallery"
				   href="images/works/DSCF0214.jpg">
					<img class="gallery_img" src="images/works/DSCF0214.jpg"></a>
			</div>

		</div>

		<div class="row mb-5">
			<div class="col-md-6">
				<a data-fancybox="gallery"
				   href="images/works/DSCF0277.jpg">
					<img class="gallery_img" src="images/works/DSCF0277.jpg"></a>
			</div>
			<div class="col-md-6">
				<a data-fancybox="gallery"
				   href="images/works/DSCF0279.jpg">
					<img class="gallery_img" src="images/works/DSCF0279.jpg"></a>
			</div>

		</div>

		<div class="row mb-5">
			<div class="col-md-6">
				<a data-fancybox="gallery"
				   href="images/works/DSCF0281.jpg">
					<img class="gallery_img" src="images/works/DSCF0281.jpg"></a>
			</div>
			<div class="col-md-6">
				<a data-fancybox="gallery"
				   href="images/works/DSCF0282.jpg">
					<img class="gallery_img" src="images/works/DSCF0282.jpg"></a>
			</div>

		</div>
	</div>


</div>

@include('footer.footer')

@extends('layouts.main_app')
<div class="container ">
    <div class="row pt-2 mt-4 no-gutters">


        <div class="col-md-2">
            <div class="block-2">
                <a class="active-2" href="{{ url('/') }}">ПАРКЕТ</a>
                <div class="dropdown-content">
                    <a class="active-2" href="{{ url('/parketnestfloor') }}">NESTFLOOR</a>
                    <a class="active-2" href="{{ url('/parketdoob') }}">ПАРКЕТ ДУБ</a>
                    <a class="active-2" href="{{ url('/parketyasen') }}">ПАРКЕТ ЯСЕН</a>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="block-2">
                <a class="active-2" href="{{ url('/') }}"> ПАРКЕТНА ДОШКА</a>
                <div class="dropdown-content">
                    <a class="active-2" href="{{ url('/baltikwood') }}">BALTIKWOOD</a>
                    <a class="active-2" href="{{ url('/nestfloor') }}">NESTFLOOR</a>
                    <a class="active-2" href="{{ url('/arcobaleno') }}">ARCOBALENO</a>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="block-2">
                <a class="active-2" href="{{ url('/') }}">МАСИВНА ДОШКА</a>
                <div class="dropdown-content">
                    <a class="active-2" href="{{ url('/massiv.doshka') }}">NESTFLOOR</a>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="block-2">
                <a class="active-2" href="{{ url('/') }}">ЛАМІНАТ</a>
                <div class="dropdown-content">
                    <a class="active-2" href="{{ url('/quickstep') }}">QUICK-STEP</a>
                    <a class="active-2" href="{{ url('/agt') }}">AGT</a>
                    <a class="active-2" href="{{ url('/alsapan') }}">ALSAPAN</a>
                    <a class="active-2" href="{{ url('/hdm') }}">HDM</a>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="block-2">
                <a class="active-2" href="{{ url('/') }}">ТЕРАСНА ДОШКА</a>
                <div class="dropdown-content">
                    <a class="active-2" href="{{ url('/terass') }}">HOLZDORF</a>
                </div>
            </div>
        </div>



        <div class="col-md-2">
            <div class="block-2">
                <a class="active-2" href="{{ url('/') }}">ПАРКЕТНА ХІМІЯ</a>
                <div class="dropdown-content">
                    <a class="active-2" href="{{ url('/kley') }}">КЛЕЙ</a>
                    <a class="active-2" href="{{ url('/lak') }}">ЛАК</a>
                    <a class="active-2" href="{{ url('/maslo') }}">МАСЛО</a>
                    <a class="active-2" href="{{ url('/shpaklivka') }}">ШПАКЛІВКА</a>
                    <a class="active-2" href="{{ url('/grunt') }}">ГРУНТІВКА</a>
                    <a class="active-2" href="{{ url('/instrument') }}">ІНСТРУМЕНТ</a>
                    <a class="active-2" href="{{ url('/doglad') }}">ДОГЛЯД</a>
                    <a class="active-2" href="{{ url('/material') }}">ВИТРАТНИЙ МАТЕРІАЛ</a>
                </div>
            </div>
        </div>


    </div>
</div>

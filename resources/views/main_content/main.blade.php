@include('main_content.menu')

<div class="content mt-4">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators" style="display: none">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
               <a href="http://svitparketu.vn.ua/agt"><img class="d-block w-100" src="{{ asset('../images/action/action_1.jpg') }}" alt="First slide"></a>
            </div>
            <div class="carousel-item">
                <a href="http://svitparketu.vn.ua/baltikwood"><img class="d-block w-100" src="{{ asset('../images/action/action_2.jpg') }}" alt="Second slide"></a>
            </div>
            <div class="carousel-item">
                <a href="http://svitparketu.vn.ua/kley"><img class="d-block w-100" src="{{ asset('../images/action/action_3.jpg') }}" alt="Third slide"></a>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span
                    class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span>
        </a> <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <span
                    class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
    </div>
</div>


<div class="content mt-3 text center" style="width: 100%">
    @foreach($news as $new)
        <div class="container text-center mt-5" style="font-size: 18px">
            <div class="row">
                <div class="col-md-12">
                    <h2>{!! $new->tittle !!}</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12" style="overflow-wrap: break-word">
                   <p align="justify"> {!! $new->description !!} </p>
                </div>
            </div>
        </div>
    @endforeach
</div>
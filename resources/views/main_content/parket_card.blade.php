@if (session('status'))
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <center><strong>{{ session('status') }}</strong></center>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="container-fluid projects-fluid mt-5">
    <div class="row card-group mt-1 ml-1 mr-1">
        @foreach($parket as $park)
            @if($park->discount > 0)
                <div class="col-sm-4">
                    <div class="card col-xl-12" style="background: transparent; border: none">
                        <div class="card-body">
                            <div class="row no-gutters">
                                <div class="col-md-12 text-center animated slideInUp">
                                    <img data-toggle="modal" data-target="#{{$park->id}}"
                                         style="min-height: 100%; cursor: pointer; border: 2px solid" src="{{ asset("images/items/$park->image") }}"> <img data-toggle="modal"
                                                                                              data-target="#{{$park->id}}"
                                                                                              style="max-width: 50%; position: absolute; top:-5%;left:45%;"
                                                                                              src="{{ asset("images/discount.png") }}">
                                </div>
                            </div>

                            @if($park->collection)
                                <div class="row card-text align-items-center mt-3">
                                    <div class="col md-12 card-title text-center">
                                        <h5>{{$park->name}}</h5>
                                        <h6>{{'Колекція: '}}{{$park->collection}}</h6>
                                    </div>
                                </div>
                            @else
                                <div class="row card-text align-items-center mt-3">
                                    <div class="col md-12 card-title text-center">
                                        <h5>{{$park->name}}</h5>
                                    </div>
                                </div>
                            @endif

                            @if($park->EUR > 0)
                                <div class="row justify-content-center">
                                    <button type="button" class="batton" data-toggle="modal"
                                            data-target="#{{$park->id}}">
                                        <div style="text-decoration: line-through; font-size: 16px; margin-right: 10%">
                                            {{round($park->EUR * $kurs->EUR)}}
                                        </div>
                                        <div style="font-size: 18px; color: #fd7e14">
                                            {{round($park->EUR * $kurs->EUR - ($park->EUR * $kurs->EUR * (($park->discount)/100)))}}{{' грн'}}
                                        </div>
                                    </button>
                                </div>
                            @else
                                <div class="row justify-content-center">
                                    <button type="button" class="batton" data-toggle="modal"
                                            data-target="#{{$park->id}}">
                                        <div style="text-decoration: line-through; font-size: 16px; margin-right: 10%">
                                            {{$park->UAN}}
                                        </div>
                                        <div style="font-size: 18px; color: #fd7e14">
                                            {{round($park->UAN - ($park->UAN * (($park->discount)/100)))}}{{' грн'}}
                                        </div>
                                    </button>
                                </div>
                            @endif

                                <!--                WITHOUT COUNT               -->
                            @else
                                <div class="col-sm-4">
                                    <div class="col-xl-12" style="background: transparent; border: none">
                                        <div class="card-body">
                                            <div class="row no-gutters">
                                                <div class="col-md-12 text-center animated fadeIn">
                                                    <div class="effect_fade">
                                                    <img data-toggle="modal" class="image" data-target="#{{$park->id}}"
                                                         style="max-width: 100%; cursor: pointer; border: 2px solid;"
                                                         src="{{ asset("images/items/$park->image") }}">
                                                    </div>
                                                </div>
                                            </div>

                                            @if($park->collection)
                                                <div class="row card-text align-items-center mt-3">
                                                    <div class="col md-12 card-title text-center">
                                                        <h5>{{$park->name}}</h5>
                                                        <h6>{{'Колекція: '}}{{$park->collection}}</h6>
                                                    </div>
                                                </div>
                                                @else
                                                <div class="row card-text align-items-center mt-3">
                                                    <div class="col md-12 card-title text-center">
                                                        <h5>{{$park->name}}</h5>
                                                    </div>
                                                </div>
                                            @endif

                                            @if($park->EUR > 0)
                                                <div class="row justify-content-center">
                                                    <button type="button" class="batton" data-toggle="modal"
                                                            data-target="#{{$park->id}}">{{round($park->EUR * $kurs->EUR)}}{{' грн'}}</button>
                                                </div>
                                            @else
                                                <div class="row justify-content-center animated fadeIn">
                                                    <button type="button" class="batton" data-toggle="modal"
                                                            data-target="#{{$park->id}}">{{$park->UAN}}{{' грн'}}</button>
                                                </div>
                                            @endif
                                            @endif


                                            <!--                Admin  Buttons                  -->


                                            @can('admin_section')
                                                <div class="row justify-content-center mt-4">
                                                    <form method="POST"
                                                          action="{{ action('AdminController@update') }}">
                                                        <input type="hidden" name="item_id" value="{{$park->id}}">
                                                        <button type="sumbit" class="btn btn-outline-info btn-sm">
                                                            Edit
                                                        </button>
                                                        {{ csrf_field() }}
                                                    </form>
                                                </div>
                                                <div class="row justify-content-center">
                                                    <form method="POST"
                                                          action="{{ action('AdminController@delete_parket') }}">
                                                        <input type="hidden" name="item_id" value="{{$park->id}}">
                                                        <button type="sumbit" class="btn btn-outline-danger btn-sm">
                                                            Delete
                                                        </button>
                                                        {{ csrf_field() }}
                                                    </form>
                                                </div>

                                                <div class="row justify-content-center">
                                                    <form method="GET"
                                                          action="{{ action('AdminController@discount_index_item') }}">
                                                        <input type="hidden" name="item_id" value="{{$park->id}}">
                                                        <button type="sumbit" class="btn btn-outline-info btn-sm">
                                                            Акційна знижка
                                                        </button>
                                                        {{ csrf_field() }}
                                                    </form>
                                                </div>

                                        @endcan

                                        <!--             Modal Window                    -->

                                            <div class="modal fade" id="{{$park->id}}" tabindex="-1" role="dialog"
                                                 aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title"
                                                                id="exampleModalLongTitle">{{'Детально'}} {{$park->name}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="container" style="width: 100%">
                                                                <div class="row text-center">
                                                                    <div class="col-md-12">
                                                                        <img style="min-width: 75%;" src="{{ asset("images/items/$park->image") }}">
                                                                    </div>
                                                                    <div class="col-md-12 mb-2 mt-4">
                                                                        <h4>{{$park->description}}</h4>
                                                                    </div>
                                                                    <div class="card-body">
                                                                        @if($park->discount > 0)
                                                                            @if($park->EUR > 0)
                                                                                <div class="row justify-content-center">
                                                                                    <button type="button" class="batton"
                                                                                            data-toggle="modal"
                                                                                            data-target="#{{$park->id}}">
                                                                                        <div style="text-decoration: line-through; font-size: 16px; margin-right: 10%">
                                                                                            {{round($park->EUR * $kurs->EUR)}}
                                                                                        </div>

                                                                                        <div style="font-size: 18px; color: #fd7e14">
                                                                                            {{round($park->EUR * $kurs->EUR - ($park->EUR * $kurs->EUR * (($park->discount)/100)))}}{{' грн'}}
                                                                                        </div>
                                                                                    </button>
                                                                                </div>
                                                                            @else
                                                                                <div class="row justify-content-center">
                                                                                    <button type="button" class="batton"
                                                                                            data-toggle="modal"
                                                                                            data-target="#{{$park->id}}">
                                                                                        <div style="text-decoration: line-through; font-size: 16px; margin-right: 10%">
                                                                                            {{$park->UAN}}
                                                                                        </div>
                                                                                        <div style="font-size: 18px; color: #fd7e14">
                                                                                            {{round($park->UAN - ($park->UAN * (($park->discount)/100)))}}{{' грн'}}
                                                                                        </div>
                                                                                    </button>
                                                                                </div>
                                                                            @endif
                                                                        @else
                                                                            @if($park->EUR > 0)
                                                                                <div class="row justify-content-center">
                                                                                    <button type="button" class="batton"
                                                                                            data-toggle="modal"
                                                                                            data-target="#{{$park->id}}">{{round($park->EUR * $kurs->EUR)}}{{' грн'}}</button>
                                                                                </div>
                                                                            @else
                                                                                <div class="row justify-content-center">
                                                                                    <button type="button" class="batton"
                                                                                            data-toggle="modal"
                                                                                            data-target="#{{$park->id}}">{{$park->UAN}}{{' грн'}}</button>
                                                                                </div>
                                                                            @endif
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                        </div>
                    </div>
                </div>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-md-3 text-center mb-3"><strong>Попередня</strong></div>
    <div class="pagination-sm"> {{ $parket->links() }}</div>
    <div class="col-md-3 text-center"><strong>Наступна</strong></div>
</div>




{{--<div class="container">--}}
    {{--<img src="img_avatar.png" alt="Avatar" class="image" style="width:100%">--}}
    {{--<div class="middle">--}}
        {{--<div class="text">John Doe</div>--}}
    {{--</div>--}}
{{--</div>--}}















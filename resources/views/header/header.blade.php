<body  style="background: url(images/fon_2.jpg)"></body>
    <div class="head">
        <div class="logo animated zoomIn" style="animation-delay: 1.5s">
            <a href="{{'/'}}"><img class="logo_image" src="images/logo.jpeg"></a>
        </div>
        <div class="contact">
            <div class="adress animated bounceInRight">м. Вінниця</div>

            <div class="adress animated bounceInRight" style="animation-delay: 0.25s">вул. Київська, 16</div>

            <div class="adress animated bounceInRight" style="animation-delay: 0.5s">(0432) 55-78-81</div>

            <div class="adress animated bounceInRight" style="animation-delay: 0.75s">(063) 478 48 05</div>

            <div class="adress animated bounceInRight" style="animation-delay: 1s">Пн-Пт: 9-00 - 18-00; Сб: 10-00 - 15-00; Нд: вихідний</div>

        </div>
    </div>

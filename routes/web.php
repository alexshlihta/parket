<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['middleware' => 'can:admin_section'], function () {
    Route::get('/admin', 'AdminController@index')->name('admin');
    Route::get('/rate', 'AdminController@index_rate')->name('admin_rate');
    Route::post('/rate', 'AdminController@change_rate')->name('change_rate');
    Route::get('/category', 'AdminController@category')->name('admin_category');
    Route::get('/new_item', 'AdminController@new_item')->name('new_item');
    Route::post('/new_item', 'AdminController@delete_parket')->name('delete_parket');
    Route::any('/create', 'AdminController@create')->name('create.item');
    Route::get('/discount', 'AdminController@discount_index_item')->name('discount_index_item');
    Route::post('/discountcreate', 'AdminController@discount_create')->name('discount_create');
    Route::get('/news', 'AdminController@news_index');
    Route::post('/news', 'AdminController@news_create')->name('news.create');
    Route::post('/delete', 'AdminController@delete_news')->name('news.delete');
    Route::post('/update', 'AdminController@update')->name('update');
    Route::post('/edit', 'AdminController@edit')->name('edit');
});
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');

Route::get('/baltikwood', 'ParketController@baltikwood')->name('baltikwood');
Route::get('/nestfloor', 'ParketController@nestfloor')->name('nestfloor');
Route::get('/arcobaleno', 'ParketController@arcobaleno')->name('arcobaleno');
Route::get('/massiv.doshka', 'ParketController@massiv_doshka')->name('massiv_doshka');
Route::get('/quickstep', 'ParketController@quickstep')->name('quickstep');
Route::get('/agt', 'ParketController@agt')->name('agt');
Route::get('/alsapan', 'ParketController@alsapan')->name('alsapan');
Route::get('/terass', 'ParketController@terass')->name('terass');
Route::get('/kley', 'ParketController@kley')->name('kley');
Route::get('/lak', 'ParketController@lak')->name('lak');
Route::get('/maslo', 'ParketController@maslo')->name('maslo');
Route::get('/shpaklivka', 'ParketController@shpaklivka')->name('shpaklivka');
Route::get('/grunt', 'ParketController@grunt')->name('grunt');
Route::get('/instrument', 'ParketController@instrument')->name('instrument');
Route::get('/doglad', 'ParketController@doglad')->name('doglad');
Route::get('/material', 'ParketController@material')->name('material');
Route::get('/parketnestfloor', 'ParketController@parketnestfloor')->name('parketnestfloor');
Route::get('/parketdoob', 'ParketController@parketdoob')->name('parketdoob');
Route::get('/parketyasen', 'ParketController@parketyasen')->name('parketyasen');
Route::get('/hdm', 'ParketController@hdm')->name('hdm');



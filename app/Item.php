<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Item extends Model
{
    public function category(){
        return $this->belongsToMany('App\Category', 'category_to_items','item_id', 'category_id');
    }

}

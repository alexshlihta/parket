<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function items(){
        return $this->belongsToMany('App\Item', 'categories_to_items','category_id', 'item_id' );
    }
}

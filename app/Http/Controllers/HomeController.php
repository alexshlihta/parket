<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();
        return view('home', compact('news'));
    }
    
    public function about () {
        return view('main_content.about_us');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Category;
use App\Kurs;
use App\News;
use Intervention\Image\ImageManagerStatic as Image;
class AdminController extends Controller
{
    public function index()
    {
        return view('admin.admin');
    }
    
    public function category()
    {
        $category = Category::all();
        return view('admin.admin_category', compact('category'));
    }
    
    public function new_item()
    {
        $category = Category::all();
        return view('admin.admin_item', compact('category'));
    }

    public function delete_parket(Request $request)
    {
        $item = Item::find($request->item_id);
        $item->delete();
        return back()->with('status', 'Item Deleted');
    }
    
    public function index_rate()
    {
        $kurs = Kurs::first();
        return view('admin.admin_rate', compact('kurs'));
    }
    
    public function change_rate(Request $request)
    {
        $kurs = Kurs::first();
        $kurs->EUR = $request->EUR;
        $kurs->USD = $request->USD;
        $kurs->UAN = $request->UAN;
        $kurs->save();
        return back()->with('status', 'Курс змінено', compact('kurs'));
    }
    
    public function create (Request $request){
        $item = new Item();
        $item->name = $request->name;
        $item->description = $request->text;
        $item->EUR = $request->EUR;
        $item->UAN = $request->UAN;
        $item->category = $request->category;
        $item->collection = $request->collection;
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $photoName = time().'.'.$request->image->getClientOriginalExtension();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(193, 250);
            $image_resize->save(public_path('images/items/' .$photoName));
            $item->image = $photoName;
        }
        $item->save();
        return back()->with('status', 'Операція виконана успішно');
    }
    
    public function discount_index_item(Request $request)
    {
        $item = Item::find($request->item_id);
        return view('admin.discount', compact('item'));
    }
    
    public function discount_create(Request $request)
    {
        $item = Item::find($request->item_id);
        $item ->discount = $request->discount;
        $item ->save();
        return back()->with('status', 'Знижку встановлено');
    }
    
    public function news_index()
    {
        $news = News::all();
        return view('admin.admin_news', compact('news'));
    }
    
    public function news_create(Request $request)
    {
        $item = new News();
        $item ->tittle = $request->tittle;
        $item ->description = $request->description;
        $item ->save();
        return back()->with('status', 'Новину добавлено');
    }
    
    public function delete_news(Request $request)
    {
        $news = News::find($request->news_id);
        $news->delete();
        return back()->with('status', 'Новину знищено');
    }
    
    public function update(Request $request)
    {
        $old_url = url()->previous();
        $item = Item::find($request->item_id);
        $category = Category::all();
        $item_category = Category::where('id',$item->category)->first()->name;
        return view('admin.admin_item_update', compact('item','category','item_category','old_url'));
    }
    
    public function edit(Request $request)
    {
        $url = $request->old_url;
        $item = Item::find($request->item_id);
        $item->name = $request->name;
        $item->description = $request->text;
        $item->collection = $request->collection;
        $item->EUR = $request->EUR;
        $item->UAN = $request->UAN;
        if($request->category){
            $item->category = $request->category;
        }
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $photoName = time().'.'.$request->image->getClientOriginalExtension();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(193, 250);
            $image_resize->save(public_path('images/items/' .$photoName));
            $item->image = $photoName;
        }
        $item->update();
        return redirect($url);
    }
    
}
//
//
//if($request->image){
//    $photoName = time().'.'.$request->image->getClientOriginalExtension();
//    $request->image->move(public_path('images/items/'), $photoName);
//    $item->image = $photoName;
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Kurs;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
class ParketController extends Controller
{
    public function baltikwood(Request $request)
    {
        $kurs = Kurs::first();
        $old_parket = Item::where('category', 1)->get();
        foreach ($old_parket as $col)
        {
            if($col->EUR > 0)
            {
                $col->UAN = $col->EUR * $kurs->EUR;
            }
        }
        $parket = $old_parket->sortBy('UAN');
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($parket);
        $perPage = 9;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        return view('parket.baltikwood', ['parket' => $paginatedItems, 'kurs' => $kurs]);
    }
    
    public function nestfloor(Request $request)
    {
        $kurs = Kurs::first();
        $old_parket = Item::where('category', 2)->get();
        foreach ($old_parket as $col)
        {
            if($col->EUR > 0)
            {
                $col->UAN = $col->EUR * $kurs->EUR;
            }
        }
        $parket = $old_parket->sortBy('UAN');
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($parket);
        $perPage = 9;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        return view('parket.nestfloor', ['parket' => $paginatedItems, 'kurs' => $kurs]);
    }
    
    public function arcobaleno(Request $request)
    {
        $kurs = Kurs::first();
        $old_parket = Item::where('category', 3)->get();
        foreach ($old_parket as $col)
        {
            if($col->EUR > 0)
            {
                $col->UAN = $col->EUR * $kurs->EUR;
            }
        }
        $parket = $old_parket->sortBy('UAN');
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($parket);
        $perPage = 9;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        return view('parket.arcobaleno', ['parket' => $paginatedItems, 'kurs' => $kurs]);
    }
    
    public function massiv_doshka(Request $request)
    {
        $kurs = Kurs::first();
        $old_parket = Item::where('category', 4)->get();
        foreach ($old_parket as $col)
        {
            if($col->EUR > 0)
            {
                $col->UAN = $col->EUR * $kurs->EUR;
            }
        }
        $parket = $old_parket->sortBy('UAN');
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($parket);
        $perPage = 9;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        return view('parket.massiv_doshka', ['parket' => $paginatedItems, 'kurs' => $kurs]);
    }
    
    public function quickstep(Request $request)
    {
        $kurs = Kurs::first();
        $old_parket = Item::where('category', 5)->get();
        foreach ($old_parket as $col)
        {
            if($col->EUR > 0)
            {
                $col->UAN = $col->EUR * $kurs->EUR;
            }
        }
        $parket = $old_parket->sortBy('UAN');
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($parket);
        $perPage = 9;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        return view('parket.quickstep', ['parket' => $paginatedItems, 'kurs' => $kurs]);
    }
    
    public function agt(Request $request)
    {
        $kurs = Kurs::first();
        $old_parket = Item::where('category', 6)->get();
        foreach ($old_parket as $col)
        {
            if($col->EUR > 0)
            {
                $col->UAN = $col->EUR * $kurs->EUR;
            }
        }
        $parket = $old_parket->sortBy('UAN');
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($parket);
        $perPage = 9;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        return view('parket.agt', ['parket' => $paginatedItems, 'kurs' => $kurs]);
    }
    
    public function alsapan(Request $request)
    {
        $kurs = Kurs::first();
        $old_parket = Item::where('category', 7)->get();
        foreach ($old_parket as $col)
        {
            if($col->EUR > 0)
            {
                $col->UAN = $col->EUR * $kurs->EUR;
            }
        }
        $parket = $old_parket->sortBy('UAN');
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($parket);
        $perPage = 9;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        return view('parket.alsapan', ['parket' => $paginatedItems, 'kurs' => $kurs]);
    }
    
    public function terass(Request $request)
    {
        $kurs = Kurs::first();
        $old_parket = Item::where('category', 8)->get();
        foreach ($old_parket as $col)
        {
            if($col->EUR > 0)
            {
                $col->UAN = $col->EUR * $kurs->EUR;
            }
        }
        $parket = $old_parket->sortBy('UAN');
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($parket);
        $perPage = 9;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        return view('parket.terass', ['parket' => $paginatedItems, 'kurs' => $kurs]);
    }
    
    public function kley()
    {
        $parket = Item::where('category', 9)->paginate(12);
        $kurs = Kurs::first();
        return view('parket.kley', compact('parket','kurs'));
    }
    
    public function lak()
    {
        $parket = Item::where('category', 10)->paginate(12);
        $kurs = Kurs::first();
        return view('parket.lak', compact('parket','kurs'));
    }
    
    public function maslo()
    {
        $parket = Item::where('category', 11)->paginate(12);
        $kurs = Kurs::first();
        return view('parket.maslo', compact('parket','kurs'));
    }
    
    public function shpaklivka()
    {
        $parket = Item::where('category', 12)->paginate(12);
        $kurs = Kurs::first();
        return view('parket.shpaklivka', compact('parket','kurs'));
    }
    
    public function grunt()
    {
        $parket = Item::where('category', 13)->paginate(12);
        $kurs = Kurs::first();
        return view('parket.grunt', compact('parket','kurs'));
    }
    
    public function instrument()
    {
        $parket = Item::where('category', 14)->paginate(12);
        $kurs = Kurs::first();
        return view('parket.instrument', compact('parket','kurs'));
    }
    
    public function doglad()
    {
        $parket = Item::where('category', 15)->paginate(12);
        $kurs = Kurs::first();
        return view('parket.doglad', compact('parket','kurs'));
    }
    
    public function material()
    {
        $parket = Item::where('category', 16)->paginate(12);
        $kurs = Kurs::first();
        return view('parket.material', compact('parket','kurs'));
    }
    
    public function parketnestfloor(Request $request)
    {
        $kurs = Kurs::first();
        $old_parket = Item::where('category', 17)->get();
        foreach ($old_parket as $col)
        {
            if($col->EUR > 0)
            {
                $col->UAN = $col->EUR * $kurs->EUR;
            }
        }
        $parket = $old_parket->sortBy('UAN');
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($parket);
        $perPage = 9;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        return view('parket.parketnestfloor', ['parket' => $paginatedItems, 'kurs' => $kurs]);
    }
    
    public function parketdoob(Request $request)
    {
        $kurs = Kurs::first();
        $old_parket = Item::where('category', 18)->get();
        foreach ($old_parket as $col)
        {
            if($col->EUR > 0)
            {
                $col->UAN = $col->EUR * $kurs->EUR;
            }
        }
        $parket = $old_parket->sortBy('UAN');
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($parket);
        $perPage = 9;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        return view('parket.parketdoob', ['parket' => $paginatedItems, 'kurs' => $kurs]);
    }
    
    public function hdm(Request $request)
    {
        $kurs = Kurs::first();
        $old_parket = Item::where('category', 20)->get();
        foreach ($old_parket as $col)
        {
            if($col->EUR > 0)
            {
                $col->UAN = $col->EUR * $kurs->EUR;
            }
        }
        $parket = $old_parket->sortBy('UAN');
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($parket);
        $perPage = 9;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        return view('parket.hdm', ['parket' => $paginatedItems, 'kurs' => $kurs]);
    }
    
    public function parketyasen(Request $request)
    {
        $kurs = Kurs::first();
        $old_parket = Item::where('category', 19)->get();
        foreach ($old_parket as $col)
        {
            if($col->EUR > 0)
            {
                $col->UAN = $col->EUR * $kurs->EUR;
            }
        }
        $parket = $old_parket->sortBy('UAN');
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($parket);
        $perPage = 9;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        return view('parket.parketyasen', ['parket' => $paginatedItems, 'kurs' => $kurs]);
    }

}

<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['name' => 'Паркетна дошка BaltikWood'],
            ['name' => 'Паркетна дошка NestFloor'],
            ['name' => 'Паркетна дошка Arcobaleno'],
            ['name' => 'Масивна дошка'],
            ['name' => 'Ламінат Quick-Step'],
            ['name' => 'Ламінат AGT'],
            ['name' => 'Ламінат Alsapan'],
            ['name' => 'Терасна дошка'],
            ['name' => 'Клей'],
            ['name' => 'Лак'],
            ['name' => 'Масло'],
            ['name' => 'Шпаклівка'],
            ['name' => 'Грунтівка'],
            ['name' => 'Інструмент'],
            ['name' => 'Догляд'],
            ['name' => 'Витратний матеріал'],
            ['name' => 'Паркет NestFloor'],
            ['name' => 'Паркет Дуб'],
            ['name' => 'Паркет Ясен'],
            ]);
    }
}


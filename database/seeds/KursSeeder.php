<?php

use Illuminate\Database\Seeder;
use App\Kurs;
class KursSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kurs = new Kurs();
        $kurs->EUR='1';
        $kurs->UAN='1';
        $kurs->USD='1';
        $kurs->save();
    }
}
